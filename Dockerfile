FROM node:11 as base

RUN mkdir /app
WORKDIR /app
RUN cd /app
COPY . .

RUN yarn
CMD [ "yarn", "start" ]