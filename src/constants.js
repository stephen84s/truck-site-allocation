export const APPLICATION = 'truck-parking';
export const DEFAULT_PORT = 3000;
export const DB_FILE = 'trucks.sqlite';
export const DEFAULT_LOG_LEVEL = 'error';
export const DEFAULT_DB_FILE = 'trucks.sqlite';
export const TRUCK_TYPES = ['AGITATOR', 'TANKER', 'TIPPER'];
