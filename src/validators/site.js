import Joi from 'joi';

const schema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    name: Joi.string().required(),
    capacity: Joi.number()
      .integer()
      .required(),
    location: Joi.object()
      .keys({
        latitude: Joi.number().required(),
        longitude: Joi.number().required()
      })
      .required()
  });

const validateContent = body => {
  return Joi.validate(body, schema, err => {
    if (err) {
      return err.details.map(error => ({
        detail: error.message,
        path: error.path.join('.')
      }));
    }
  });
};

export default validateContent;
