import BaseJoi from 'joi';
import JoiExtension from 'joi-date-extensions';
import { TRUCK_TYPES } from '../constants';

const Joi = BaseJoi.extend(JoiExtension);

const schema = Joi.object()
  .options({ abortEarly: false })
  .keys({
    truckType: Joi.string()
      .valid(TRUCK_TYPES)
      .required(),
    numberPlate: Joi.string()
      .regex(/^[A-Za-z]{3}-[\d]{3}$/)
      .required(),

    // Rego Date can't be greater than today
    regoDate: Joi.date()
      .format('YYYY-MM-DD')
      .max('now')
      .required(),

    // Although the values might be too low / high, have no clue about trucks
    // so keeping them somewhere reasonable ?
    tonnes: Joi.number()
      .integer()
      .min(5)
      .max(500)
  });

const validateContent = body => {
  return Joi.validate(body, schema, err => {
    if (err) {
      return err.details.map(error => ({
        detail: error.message,
        path: error.path.join('.')
      }));
    }
  });
};

export default validateContent;
