import validateContent from './truck';

describe('validateContent truck', () => {
  describe('when no errors are encountered', () => {
    const body = {
      truckType: 'TIPPER',
      numberPlate: 'XXX-111',
      regoDate: '2018-01-01',
      tonnes: 25
    };

    it('should return undefined', () => {
      expect(validateContent(body)).toBeUndefined();
    });
  });

  describe('when errors are encountered', () => {
    describe('when truckType is invalid', () => {
      const body = {
        truckType: 'TIPPERS',
        numberPlate: 'XXX-111',
        regoDate: '2018-01-01',
        tonnes: 25
      };

      it('should return error', () => {
        expect(validateContent(body)[0].detail).toContain('truckType');
      });
    });

    describe('when tonnes is invalid', () => {
      const body = {
        truckType: 'TIPPER',
        numberPlate: 'XXX-111A',
        regoDate: '2018-01-01',
        tonnes: 25
      };

      it('should return error', () => {
        expect(validateContent(body)[0].detail).toContain('numberPlate');
      });
    });

    describe('when regoDate is invalid', () => {
      const body = {
        truckType: 'TIPPER',
        numberPlate: 'XXX-111',
        regoDate: '2099-01-01',
        tonnes: 25
      };

      it('should return error', () => {
        expect(validateContent(body)[0].detail).toContain('regoDate');
      });
    });
  });
});
