import validateContent from './site';

describe('validateContent site', () => {
  describe('when no errors are encountered', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        latitude: '10',
        longitude: '20'
      }
    };

    it('should return undefined', () => {
      expect(validateContent(body)).toBeUndefined();
    });
  });

  describe('when errors are encountered', () => {
    describe('when name is empty', () => {
      const body = {
        name: '',
        capacity: '2',
        location: {
          latitude: '10',
          longitude: '20'
        }
      };

      it('should return error', () => {
        expect(validateContent(body)[0].detail).toContain('name');
      });
    });
  });

  describe('when capacity is empty', () => {
    const body = {
      name: 'Site 1',
      capacity: '',
      location: {
        latitude: '10',
        longitude: '20'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('capacity');
    });
  });

  describe('when capacity is not a number', () => {
    const body = {
      name: 'Site 1',
      capacity: 'Two',
      location: {
        latitude: '10',
        longitude: '20'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('capacity');
    });
  });

  describe('when latitude attribute is missing', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        longitude: '20'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('latitude');
    });
  });

  describe('when latitude is empty', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        latitude: '',
        longitude: '20'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('latitude');
    });
  });

  describe('when latitute is not a number', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        latitude: 'Ten',
        longitude: '20'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('latitude');
    });
  });

  describe('when longitude attribute is missing', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        latitude: '10'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('longitude');
    });
  });

  describe('when longitude is empty', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        latitude: '10',
        longitude: ''
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('longitude');
    });
  });

  describe('when longitude is not a number', () => {
    const body = {
      name: 'Site 1',
      capacity: '2',
      location: {
        latitude: '10',
        longitude: 'Twenty'
      }
    };

    it('should return error', () => {
      expect(validateContent(body)[0].detail).toContain('longitude');
    });
  });
});
