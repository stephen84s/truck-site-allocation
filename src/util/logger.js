import bunyan from 'bunyan';

import { DEFAULT_LOG_LEVEL } from '../constants';

import { APPLICATION } from '../constants';

const createLogger = () =>
  bunyan.createLogger({
    name: APPLICATION,
    stream: process.stdout,
    level: process.env.LOG_LEVEL || DEFAULT_LOG_LEVEL
  });

export default createLogger();
