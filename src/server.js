import express from 'express';
import bodyParser from 'body-parser';
import bunyanMiddleware from 'bunyan-middleware';

import routes from './routes';

const createServer = logger => {
  const app = express();

  app.use(bodyParser.json());
  app.use(bunyanMiddleware({ logger }));

  app.use(routes);

  return app;
};

export default createServer;
