import daoLayer from './daoLayer';
import TruckSite from '../model/TruckSite';
import Truck from '../model/Truck';
import Site from '../model/Site';

class TruckSiteRepo {
  async add(truckSite) {
    if (truckSite instanceof TruckSite) {
      try {
        const result = await daoLayer.run(
          'INSERT INTO TruckSites(truckId, siteId, allocationDate) values (?,?,?)',
          [truckSite.truck.id, truckSite.site.id, truckSite.allocationDate]
        );

        return result.changes;
      } catch (err) {
        throw new Error(err.message);
      }
    }
    throw new Error('Can only add TruckSites');
  }

  async update(truckSites) {
    if (truckSites instanceof TruckSite) {
      const result = await daoLayer.run(
        'UPDATE TruckSites SET allocationDate = ?, siteId = ? where truckId = ?',
        [truckSites.allocationDate, truckSites.site.id, truckSites.truck.id]
      );
      return result.changes;
    }
    throw new Error('Can only update TruckSites');
  }

  async findTruckSiteByTruckId(id) {
    try {
      return await daoLayer.get('SELECT * FROM TruckSites WHERE truckId = ?', [
        id
      ]);
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findTrucksAllocatedToSite(siteId) {
    try {
      const results = await daoLayer.all(
        'SELECT t.* FROM TruckSites ts INNER JOIN Trucks t ON ts.truckId = t.id WHERE siteId = ?',
        siteId
      );

      if (results) {
        return results.map(result => new Truck(result));
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findTrucksToReallocate(checkDate) {
    try {
      const results = await daoLayer.all(
        'SELECT t.* FROM TruckSites ts INNER JOIN Trucks t ON ts.truckId = t.id WHERE date(ts.allocationDate) <= date(?, "-2 days")',
        [`${checkDate}`]
      );

      if (results) {
        return results.map(result => new Truck(result));
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findAllTruckSites() {
    try {
      const results = await daoLayer.all(
        'SELECT t.*, s.*, ts.allocationDate FROM TruckSites ts INNER JOIN Trucks t ON ts.truckId = t.id INNER JOIN Sites s ON ts.siteId = s.id'
      );

      if (results) {
        return results.map(
          ({
            numberPlate,
            regoDate,
            tonnes,
            truckType,
            id,
            name,
            latitude,
            longitude,
            capacity,
            allocationDate
          }) => {
            const truck = new Truck({
              numberPlate,
              regoDate,
              tonnes,
              truckType
            });

            const site = new Site({
              id,
              name,
              capacity,
              location: {
                latitude,
                longitude
              }
            });

            return new TruckSite(truck, site, allocationDate);
          }
        );
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async deleteTruckSite(truckId) {
    try {
      const result = await daoLayer.run(
        'DELETE FROM TruckSites WHERE truckId = ?',
        [truckId]
      );
      return result.changes;
    } catch (err) {
      throw new Error(err.message);
    }
  }
}

export default new TruckSiteRepo();
