import sqlite from 'sqlite';

import { DEFAULT_DB_FILE } from '../constants';
import logger from '../util/logger';

class DAOLayer {
  constructor() {
    this.db = null;
  }

  async init() {
    const { NODE_ENV, DB_FILE } = process.env;
    this.db = await sqlite.open(DB_FILE || DEFAULT_DB_FILE);
    const dbMigrateParams = NODE_ENV === 'development' ? { force: 'last' } : {};
    await this.db.migrate(dbMigrateParams);
  }

  async run(sql, params = []) {
    return this.db.run(sql, ...params);
  }

  async all(sql, params = []) {
    return this.db.all(sql, ...params);
  }

  async get(sql, params = []) {
    logger.debug(`SQL ${sql} params ${params}`);
    const result = await this.db.get(sql, ...params);
    logger.debug(result);
    return result;
  }
}

export default new DAOLayer();
