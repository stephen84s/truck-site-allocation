import daoLayer from './daoLayer';
import sqlite from 'sqlite';
import { DEFAULT_DB_FILE } from '../constants';

jest.mock('sqlite3');

jest.mock('sqlite', () => ({
  open: jest.fn().mockImplementation(() => ({
    migrate: jest.fn(),
    run: jest.fn(),
    all: jest.fn()
  }))
}));

describe('daoLayer', () => {
  let origEnvVars = {};

  beforeAll(() => {
    origEnvVars = {
      DB_FILE: process.env.DB_FILE,
      NODE_ENV: process.env.NODE_ENV
    };
  });

  process.env.DB_FILE = 'Alpha.sqlite';
  process.env.NODE_ENV = 'x';

  afterEach(() => {
    jest.resetAllMocks();
  });

  afterAll(() => {
    process.env = { ...process.env, ...origEnvVars };
  });

  describe('init', () => {
    let migrateMock = null;

    beforeEach(() => {
      migrateMock = jest.fn();
      sqlite.open.mockImplementation(() =>
        Promise.resolve({
          migrate: migrateMock
        })
      );
    });
    describe('when we call init', () => {
      it('should initialise the database', async () => {
        await daoLayer.init();
        expect(sqlite.open).toHaveBeenCalledWith(process.env.DB_FILE);
      });

      it('should invoke the migrations', async () => {
        await daoLayer.init();
        expect(migrateMock).toHaveBeenCalledWith({});
      });

      describe('when DB_FILE environment variable is not set', () => {
        beforeEach(() => {
          delete process.env.DB_FILE;
        });

        it(`should call open with ${DEFAULT_DB_FILE}`, async () => {
          await daoLayer.init();
          expect(sqlite.open).toHaveBeenCalledWith(DEFAULT_DB_FILE);
        });
      });

      describe('when NODE_ENV is set as development', () => {
        let nodeEnv;
        beforeEach(() => {
          nodeEnv = process.env.NODE_ENV;
          process.env.NODE_ENV = 'development';
        });

        afterEach(() => {
          process.env.NODE_ENV = nodeEnv;
        });

        it('should force running last migration', async () => {
          await daoLayer.init();
          expect(migrateMock).toHaveBeenCalledWith({ force: 'last' });
        });
      });
    });
  });

  describe('run', () => {
    let runMock;
    const expectedQuery = 'INSERT INTO Trucks()';
    const expectedQueryParams = [1, 2, 3, 4];

    beforeEach(async () => {
      runMock = jest.fn();
      sqlite.open.mockImplementation(() =>
        Promise.resolve({
          migrate: jest.fn(),
          run: runMock
        })
      );

      await daoLayer.init();
    });

    it('should invoke run method of db connection', async () => {
      await daoLayer.run(expectedQuery, expectedQueryParams);
      expect(runMock).toHaveBeenCalledWith(
        expectedQuery,
        ...expectedQueryParams
      );
    });

    describe('when no sql query params are provided', () => {
      it('should invoke the run method with just the query', async () => {
        await daoLayer.run(expectedQuery);
        expect(runMock).toHaveBeenCalledWith(expectedQuery);
      });
    });
  });

  describe('all', () => {
    let allMock;
    const expectedQuery = 'Select * from trucks';
    const expectedQueryParams = [1, 2, 3, 4];

    beforeEach(async () => {
      allMock = jest.fn();
      sqlite.open.mockImplementation(() =>
        Promise.resolve({
          migrate: jest.fn(),
          all: allMock
        })
      );

      await daoLayer.init();
    });

    it('should invoke run method of db connection', async () => {
      await daoLayer.all(expectedQuery, expectedQueryParams);
      expect(allMock).toHaveBeenCalledWith(
        expectedQuery,
        ...expectedQueryParams
      );
    });

    describe('when no sql query params are provided', () => {
      it('should invoke the run method with just the query', async () => {
        await daoLayer.all(expectedQuery);
        expect(allMock).toHaveBeenCalledWith(expectedQuery);
      });
    });
  });

  describe('get', () => {
    let getMock;
    const expectedQuery = 'Select * from trucks';
    const expectedQueryParams = [1, 2, 3, 4];

    beforeEach(async () => {
      getMock = jest.fn();
      sqlite.open.mockImplementation(() =>
        Promise.resolve({
          migrate: jest.fn(),
          get: getMock
        })
      );

      await daoLayer.init();
    });

    it('should invoke get method of db connection', async () => {
      await daoLayer.get(expectedQuery, expectedQueryParams);
      expect(getMock).toHaveBeenCalledWith(
        expectedQuery,
        ...expectedQueryParams
      );
    });

    describe('when no sql query params are provided', () => {
      it('should invoke the get method with just the query', async () => {
        await daoLayer.get(expectedQuery);
        expect(getMock).toHaveBeenCalledWith(expectedQuery);
      });
    });
  });
});
