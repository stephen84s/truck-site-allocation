import daoLayer from './daoLayer';
import Site from '../model/Site';
import logger from '../util/logger';

class SiteRepo {
  async add(site) {
    if (site instanceof Site) {
      try {
        const result = await daoLayer.run(
          'INSERT INTO Sites(name, latitude, longitude, capacity) VALUES (?, ?, ?, ?)',
          [
            site.name,
            site.location.latitude,
            site.location.longitude,
            site.capacity
          ]
        );

        logger.info('Created Site');
        return result.lastID;
      } catch (err) {
        throw new Error(err.message);
      }
    }
    throw new Error('Can only add Sites');
  }

  async update(site) {
    if (site instanceof Site) {
      const result = await daoLayer.run(
        'UPDATE Sites SET name = ?, latitude = ?, longitude = ?, capacity = ? WHERE id = ?',
        [
          site.name,
          site.location.latitude,
          site.location.longitude,
          site.capacity,
          site.id
        ]
      );

      return result.changes;
    }
    throw new Error('Can only update Sites');
  }

  async findSiteById(id) {
    try {
      const result = await daoLayer.get('SELECT * FROM Sites WHERE id = ?', [
        id
      ]);
      if (result) {
        return new Site({
          id: result.id,
          location: {
            latitude: result.latitude,
            longitude: result.longitude
          },
          name: result.name,
          capacity: result.capacity
        });
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findSites() {
    try {
      const results = await daoLayer.all('SELECT * from Sites');
      if (results) {
        return results.map(
          ({ id, name, latitude, longitude, capacity }) =>
            new Site({
              id,
              name,
              location: {
                latitude,
                longitude
              },
              capacity
            })
        );
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async deleteSite(id) {
    try {
      const result = await daoLayer.run('DELETE FROM Sites WHERE id = ?', [id]);
      return result.changes;
    } catch (err) {
      throw new Error(err.message);
    }
  }
}

export default new SiteRepo();
