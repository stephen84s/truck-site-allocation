import daoLayer from './daoLayer';
import truckRepo from './truckRepo';
import Truck from '../model/Truck';

const lastID = 1000;
jest.mock('./daoLayer', () => ({
  run: jest.fn()
}));

describe('truckRepo', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('when creating a new truck', () => {
    describe('when passed object is not of type Truck', () => {
      it('should throw an error', async () => {
        try {
          await truckRepo.add({ a: 'a' });
          expect(true).toBe(false);
        } catch (err) {
          expect(err.message).toEqual('Can only add Trucks');
        }
      });
    });

    describe('when passed object is of type truck', () => {
      const truck = new Truck({
        numberPlate: 'XXX-111',
        type: 'TIPPER',
        regoDate: '2001-01-01',
        tonnes: 499
      });

      beforeEach(() => {
        daoLayer.run.mockReturnValue(Promise.resolve({ lastID: 1000 }));
      });

      it('should invoke run method on connection with insert statement', async () => {
        const result = await truckRepo.add(truck);
        expect(daoLayer.run).toHaveBeenCalledWith(
          'INSERT INTO Trucks(numberPLate, regoDate, tonnes, truckType) values (?,?,?,?)',
          [truck.numberPlate, truck.regoDate, truck.tonnes, truck.truckType]
        );
        expect(result).toEqual(lastID);
      });

      describe('when there is database error', () => {
        beforeEach(() => {
          daoLayer.run.mockImplementation(() => {
            throw new Error('Blah');
          });
        });
        it('should throw an error', async () => {
          try {
            await truckRepo.add(truck);
          } catch (err) {
            expect(err.message).toEqual('Blah');
          }
        });
      });
    });
  });

  describe('when updating an existing truck', () => {
    describe('when passed object is not of type Truck', () => {
      it.todo('should throw an error');
    });

    describe('when passed object is of type truck', () => {
      it.todo('should invoke run method on connection with update statement');
    });
  });

  describe('when getting an existing truck', () => {
    describe('when passed object is of type truck', () => {
      it.todo('should invoke get method on connection with select statement');
    });
  });
});
