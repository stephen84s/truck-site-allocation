import daoLayer from './daoLayer';
import Truck from '../model/Truck';

class TruckRepo {
  async add(truck) {
    if (truck instanceof Truck) {
      try {
        const result = await daoLayer.run(
          'INSERT INTO Trucks(numberPLate, regoDate, tonnes, truckType) values (?,?,?,?)',
          [truck.numberPlate, truck.regoDate, truck.tonnes, truck.truckType]
        );

        return result.lastID;
      } catch (err) {
        throw new Error(err.message);
      }
    }
    throw new Error('Can only add Trucks');
  }

  async update(truck) {
    if (truck instanceof Truck) {
      const result = await daoLayer.run(
        'UPDATE Trucks SET regoDate = ?, tonnes = ?, truckType = ?, numberPlate = ? where id = ?',
        [
          truck.regoDate,
          truck.tonnes,
          truck.truckType,
          truck.numberPlate,
          truck.id
        ]
      );

      return result.changes;
    }
    throw new Error('Can only update Trucks');
  }

  async findTruckById(id) {
    try {
      const result = await daoLayer.get('SELECT * from Trucks where id = ?', [
        id
      ]);
      if (result) {
        return new Truck(result);
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findTrucks() {
    try {
      const results = await daoLayer.all('SELECT * from Trucks');
      if (results) {
        return results.map(result => new Truck(result));
      }
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async findTruckByNumberPlate(numberPlate) {
    try {
      const result = await daoLayer.get(
        'SELECT * from Trucks where numberPlate = ?',
        [numberPlate]
      );
      if (result) {
        return new Truck(result);
      }

      return null;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async deleteTruck(id) {
    try {
      const result = await daoLayer.run('DELETE FROM Trucks WHERE id = ?', [
        id
      ]);
      return result.changes;
    } catch (err) {
      throw new Error(err.message);
    }
  }
}

export default new TruckRepo();
