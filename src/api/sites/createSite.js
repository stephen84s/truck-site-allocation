import HTTPStatus from 'http-status';
import { isEmpty } from 'lodash';

import Site from '../../model/Site';
import siteRepo from '../../db/siteRepo';
import logger from '../../util/logger';

import validateContent from '../../validators/site';

const validate = async req => {
  const validationErrors = validateContent(req.body) || [];

  if (validationErrors.length > 0) {
    req.log.error(`Validation errors. The errors are: ${validationErrors}`);
  }
  return validationErrors;
};

const createSite = async (req, res) => {
  const {
    name,
    location: { latitude, longitude },
    capacity
  } = req.body;

  const validationErrors = await validate(req);

  if (isEmpty(validationErrors)) {
    try {
      const site = new Site({
        name,
        location: { latitude, longitude },
        capacity
      });

      const createdId = await siteRepo.add(site);
      logger.debug(`Database Update Result, ${createdId}`);
      const createdSite = await siteRepo.findSiteById(createdId);
      return res.status(HTTPStatus.CREATED).send(createdSite);
    } catch (err) {
      req.log.error(`Error while adding Site to database, ${err.message}`);
      return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }
  return res.status(HTTPStatus.BAD_REQUEST).send({
    errors: validationErrors
  });
};

export default createSite;
