import HTTPStatus from 'http-status';

import siteRepo from '../../db/siteRepo';
import logger from '../../util/logger';

const deleteSite = async (req, res) => {
  const id = req.params.id;
  const siteFromDb = await siteRepo.findSiteById(id);
  if (!siteFromDb) {
    return res.sendStatus(HTTPStatus.NOT_FOUND);
  }

  try {
    const changes = await siteRepo.deleteSite(id);
    logger.debug(`Database rows affected, ${changes}`);
    return res.status(HTTPStatus.NO_CONTENT).send();
  } catch (err) {
    req.log.error(`Error while deleting Site in database, ${err.message}`);
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default deleteSite;
