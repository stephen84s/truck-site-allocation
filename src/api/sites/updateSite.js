import HTTPStatus from 'http-status';
import { isEmpty } from 'lodash';

import Site from '../../model/Site';
import siteRepo from '../../db/siteRepo';
import logger from '../../util/logger';

import validateContent from '../../validators/site';

const validate = async req => {
  const validationErrors = validateContent(req.body) || [];

  if (validationErrors.length > 0) {
    req.log.error(`Validation errors. The errors are: ${validationErrors}`);
  }
  return validationErrors;
};

const updateSite = async (req, res) => {
  const id = req.params.id;
  const {
    name,
    location: { latitude, longitude },
    capacity
  } = req.body;

  const siteInDb = await siteRepo.findSiteById(id);

  if (!siteInDb) {
    return res.sendStatus(HTTPStatus.NOT_FOUND);
  }

  const validationErrors = await validate(req);

  if (isEmpty(validationErrors)) {
    try {
      const site = new Site({
        id,
        name,
        location: { latitude, longitude },
        capacity
      });

      const changes = await siteRepo.update(site);
      logger.debug(`Database Update Result, ${changes}`);
      const updatedSite = await siteRepo.findSiteById(id);
      return res.status(HTTPStatus.OK).send(updatedSite);
    } catch (err) {
      req.log.error(`Error while updating Site in database, ${err.message}`);
      return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }
  return res.status(HTTPStatus.BAD_REQUEST).send({
    errors: validationErrors
  });
};

export default updateSite;
