import HTTPStatus from 'http-status';

import siteRepo from '../../db/siteRepo';
import logger from '../../util/logger';

const getSite = async (req, res) => {
  const id = req.params.id;
  logger.debug(`Received request to fetch Site with ID ${id}`);
  const site = await siteRepo.findSiteById(id);

  if (site) {
    return res.status(HTTPStatus.OK).send(site);
  }

  return res.sendStatus(HTTPStatus.NOT_FOUND);
};

export default getSite;
