import HTTPStatus from 'http-status';

import siteRepo from '../../db/siteRepo';

const getSites = async (req, res) => {
  try {
    const sites = await siteRepo.findSites();

    return res.status(HTTPStatus.OK).send(sites);
  } catch (err) {
    req.log.error(`Error while retrieving all sites, ${err.message}`);
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default getSites;
