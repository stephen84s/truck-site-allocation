import HTTPStatus from 'http-status';

import truckRepo from '../../db/truckRepo';

const deleteTruck = async (req, res) => {
  const id = req.params.id;

  try {
    const truckFromDb = await truckRepo.findTruckById(id);
    if (!truckFromDb) {
      return res.sendStatus(HTTPStatus.NOT_FOUND);
    }

    const changes = await truckRepo.deleteTruck(id);
    req.log.debug(`Database rows affected, ${changes}`);
    return res.sendStatus(HTTPStatus.NO_CONTENT);
  } catch (err) {
    req.log.error(`Error while deleting Truck in database, ${err.message}`);
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default deleteTruck;
