import HTTPStatus from 'http-status';

import truckRepo from '../../db/truckRepo';

const getTrucks = async (req, res) => {
  try {
    const trucks = await truckRepo.findTrucks();

    return res.status(HTTPStatus.OK).send(trucks);
  } catch (err) {
    req.log.error(`Error while retrieving all trucks, ${err.message}`);
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default getTrucks;
