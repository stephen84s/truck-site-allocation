import HTTPStatus from 'http-status';

import deleteTruck from './deleteTruck';
import truckRepo from '../../db/truckRepo';

jest.mock('../../db/truckRepo.js', () => ({
  findTruckById: jest.fn(),
  deleteTruck: jest.fn()
}));

describe('deleteTruck', () => {
  const req = {
    params: {
      id: 1
    },
    log: {
      debug: jest.fn(),
      error: jest.fn()
    }
  };

  let res = {};

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res),
      sendStatus: jest.fn()
    };
  });

  afterEach(() => jest.resetAllMocks());

  describe('when the truck to be deleted cannot be found', () => {
    beforeEach(() => {
      truckRepo.findTruckById.mockImplementation(() => undefined);
    });

    it(`should return status ${HTTPStatus.NOT_FOUND}`, async () => {
      await deleteTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(HTTPStatus.NOT_FOUND);
    });
  });

  describe('when the delete operation has an error', () => {
    beforeEach(() => {
      truckRepo.findTruckById.mockImplementation(() => {
        return {
          numberPlate: 'ABC_123'
        };
      });

      truckRepo.deleteTruck.mockImplementation(() => {
        throw new Error('Something unexpected happened');
      });
    });

    it(`should return status ${HTTPStatus.INTERNAL_SERVER_ERROR}`, async () => {
      await deleteTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(
        HTTPStatus.INTERNAL_SERVER_ERROR
      );
    });
  });

  describe('when the delete operation is a success', () => {
    beforeEach(() => {
      truckRepo.findTruckById.mockImplementation(() => {
        return {
          numberPlate: 'ABC_123'
        };
      });

      truckRepo.deleteTruck.mockImplementation(() => {
        return {
          numberPlate: 'ABC_123'
        };
      });
    });

    it(`should return status ${HTTPStatus.NO_CONTENT}`, async () => {
      await deleteTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(HTTPStatus.NO_CONTENT);
    });
  });
});
