import HTTPStatus from 'http-status';
import { isEmpty } from 'lodash';

import Truck from '../../model/Truck';
import truckRepo from '../../db/truckRepo';
import logger from '../../util/logger';

import validateContent from '../../validators/truck';

const validate = async req => {
  const validationErrors = validateContent(req.body) || [];

  const numberPlate = req.body.numberPlate;
  const truck = await truckRepo.findTruckByNumberPlate(numberPlate);
  if (truck) {
    validationErrors.push({
      detail: '"numberPlate" must be unique',
      path: 'numberPlate'
    });
  }

  if (validationErrors.length > 0) {
    req.log.error(`Validation errors. The errors are: ${validationErrors}`);
  }
  return validationErrors;
};

const createTruck = async (req, res) => {
  const { numberPlate, tonnes, regoDate, truckType } = req.body;
  const truck = new Truck({
    numberPlate,
    tonnes,
    regoDate,
    truckType
  });

  const validationErrors = await validate(req);

  if (isEmpty(validationErrors)) {
    try {
      const createdId = await truckRepo.add(truck);
      logger.debug(`Database Update Result, ${createdId}`);
      const createdTruck = await truckRepo.findTruckById(createdId);
      return res.status(HTTPStatus.CREATED).send(createdTruck);
    } catch (err) {
      req.log.error(`Error while adding Truck to database, ${err.message}`);
      return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }
  return res.status(HTTPStatus.BAD_REQUEST).send({
    errors: validationErrors
  });
};

export default createTruck;
