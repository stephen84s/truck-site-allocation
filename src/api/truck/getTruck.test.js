import HTTPStatus from 'http-status';

import getTruck from './getTruck';
import truckRepo from '../../db/truckRepo';

jest.mock('../../db/truckRepo.js', () => ({
  findTruckById: jest.fn()
}));

describe('getTruck', () => {
  const req = {
    params: {
      id: 1
    },
    log: {
      debug: jest.fn(),
      error: jest.fn()
    }
  };

  let res = {};

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res),
      sendStatus: jest.fn()
    };
  });

  afterEach(() => jest.resetAllMocks());

  describe('when the truck to be retrieved cannot be found', () => {
    beforeEach(() => {
      truckRepo.findTruckById.mockImplementation(() => undefined);
    });

    it(`should return status ${
      HTTPStatus.BAD_REQUEST
    } with the error information`, async () => {
      await getTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(HTTPStatus.NOT_FOUND);
    });
  });

  describe('when there is an error retrieving truck details', () => {
    beforeEach(() => {
      truckRepo.findTruckById.mockImplementation(() => {
        throw new Error('Something unexpected happened');
      });
    });

    it(`should return status ${HTTPStatus.INTERNAL_SERVER_ERROR}`, async () => {
      await getTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(
        HTTPStatus.INTERNAL_SERVER_ERROR
      );
    });
  });

  describe('when the the struck is successfully retrieved', () => {
    beforeEach(() => {
      truckRepo.findTruckById.mockImplementation(() => {
        return {
          numberPlate: 'ABC_123'
        };
      });
    });

    it('should return the truck details', async () => {
      await getTruck(req, res);

      expect(res.status).toHaveBeenCalledWith(HTTPStatus.OK);
      expect(res.send).toHaveBeenCalledWith({
        numberPlate: 'ABC_123'
      });
    });
  });
});
