import HTTPStatus from 'http-status';
import { isEmpty } from 'lodash';

import Truck from '../../model/Truck';
import truckRepo from '../../db/truckRepo';
import logger from '../../util/logger';

import validateContent from '../../validators/truck';

const validate = async req => {
  const validationErrors = validateContent(req.body) || [];

  const numberPlate = req.body.numberPlate;
  const truck = await truckRepo.findTruckByNumberPlate(numberPlate);
  if (truck && truck.id !== parseInt(req.params.id)) {
    validationErrors.push({
      detail: '"numberPlate" must be unique',
      path: 'numberPlate'
    });
  }

  if (validationErrors.length > 0) {
    req.log.error(`Validation errors. The errors are: ${validationErrors}`);
  }
  return validationErrors;
};

const updateTruck = async (req, res) => {
  const { numberPlate, tonnes, regoDate, truckType } = req.body;

  const truckFromDb = await truckRepo.findTruckById(req.params.id);
  if (!truckFromDb) {
    return res.sendStatus(HTTPStatus.NOT_FOUND);
  }

  const validationErrors = await validate(req);

  if (isEmpty(validationErrors)) {
    try {
      const truck = new Truck({
        ...truckFromDb,
        numberPlate,
        tonnes,
        regoDate,
        truckType
      });
      const changes = await truckRepo.update(truck);
      logger.debug(`Database rows affected, ${changes}`);
      const updatedTruck = await truckRepo.findTruckById(req.params.id);
      return res.status(HTTPStatus.OK).send(updatedTruck);
    } catch (err) {
      req.log.error(`Error while updating Truck in database, ${err.message}`);
      return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }

  return res.status(HTTPStatus.BAD_REQUEST).send({
    errors: validationErrors
  });
};

export default updateTruck;
