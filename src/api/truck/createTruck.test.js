import HTTPStatus from 'http-status';

import createTruck from './createTruck';
import truckRepo from '../../db/truckRepo';
import validateContent from '../../validators/truck';

jest.mock('../../db/truckRepo.js', () => ({
  findTruckByNumberPlate: jest.fn(),
  findTruckById: jest.fn(),
  add: jest.fn()
}));

jest.mock('../../validators/truck');

describe('createTruck', () => {
  const req = {
    body: {
      numberPlate: 'XXX-111',
      truckType: 'TIPPER',
      regoDate: '2001-01-14',
      tonnes: 100
    },
    log: {
      error: jest.fn()
    }
  };
  let res = {};

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res),
      sendStatus: jest.fn()
    };
  });

  afterEach(() => jest.resetAllMocks());

  describe('when validation fails', () => {
    describe('when incoming parameters are invalid', () => {
      const expectedError = {
        detail: '"something" is missing',
        path: 'anAttribute'
      };

      beforeEach(() => {
        validateContent.mockReturnValue(expectedError);
      });

      it(`should return status ${
        HTTPStatus.BAD_REQUEST
      } with the error information`, async () => {
        await createTruck(req, res);
        expect(res.status).toHaveBeenCalledWith(HTTPStatus.BAD_REQUEST);
        expect(res.send).toHaveBeenCalledWith({
          errors: expectedError
        });
      });
    });

    describe('when number plate is already in use', () => {
      const expectedRepoResp = {
        id: 1,
        numberPlate: 'XXX-111',
        regoDate: '2001-01-01',
        tonnes: 24,
        truckType: 'TIPPER'
      };

      beforeEach(() => {
        truckRepo.findTruckByNumberPlate.mockImplementation(
          () => expectedRepoResp
        );
      });

      it(`should return ${
        HTTPStatus.BAD_REQUEST
      } with numberPlate error`, async () => {
        await createTruck(req, res);
        expect(res.status).toHaveBeenCalledWith(HTTPStatus.BAD_REQUEST);
        expect(res.send).toHaveBeenCalledWith({
          errors: [
            {
              detail: '"numberPlate" must be unique',
              path: 'numberPlate'
            }
          ]
        });
        expect(truckRepo.add).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('when validations passes but the save operation has a success', () => {
    const truck = {
      id: 1,
      ...req.body
    };

    beforeEach(() => {
      truckRepo.findTruckByNumberPlate.mockImplementation(() => {
        return null;
      });

      truckRepo.add.mockImplementation(() => {
        throw new Error('Something went wrong');
      });
      truckRepo.findTruckById.mockImplementation(() => truck);
    });

    it('should return an internal server error', async () => {
      await createTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(
        HTTPStatus.INTERNAL_SERVER_ERROR
      );
    });
  });

  describe('when validations passes and the save operation is a success', () => {
    const newlyCreatedId = 1;
    const truck = {
      id: 1,
      ...req.body
    };

    beforeEach(() => {
      truckRepo.findTruckByNumberPlate.mockImplementation(() => {
        return null;
      });

      truckRepo.add.mockImplementation(() => newlyCreatedId);
      truckRepo.findTruckById.mockImplementation(() => truck);
    });

    it('should return the created truck', async () => {
      await createTruck(req, res);

      expect(res.status).toHaveBeenCalledWith(HTTPStatus.CREATED);
      expect(res.send).toHaveBeenCalledWith(truck);
    });
  });
});
