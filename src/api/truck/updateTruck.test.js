import HTTPStatus from 'http-status';

import updateTruck from './updateTruck';
import truckRepo from '../../db/truckRepo';
import validateContent from '../../validators/truck';

jest.mock('../../db/truckRepo.js', () => ({
  findTruckByNumberPlate: jest.fn(),
  findTruckById: jest.fn(),
  update: jest.fn()
}));

jest.mock('../../validators/truck');

describe('updateTruck', () => {
  const req = {
    body: {
      numberPlate: 'XXX-111',
      truckType: 'TIPPER',
      regoDate: '2001-01-14',
      tonnes: 100
    },
    params: {
      id: '1'
    },
    log: {
      error: jest.fn()
    }
  };

  const truck = {
    id: 1,
    ...req.body
  };

  let res = {};

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res),
      sendStatus: jest.fn()
    };

    truckRepo.findTruckById.mockImplementation(() => truck);
  });

  afterEach(() => jest.resetAllMocks());

  describe('when validation fails', () => {
    describe('when truck to be updated cannot be found', () => {
      beforeEach(() => {
        truckRepo.findTruckById.mockImplementation(() => undefined);
      });

      it(`should return status ${HTTPStatus.NOT_FOUND}`, async () => {
        await updateTruck(req, res);
        expect(res.sendStatus).toHaveBeenCalledWith(HTTPStatus.NOT_FOUND);
      });
    });

    describe('when incoming parameters are invalid', () => {
      const expectedError = {
        detail: '"something" is missing',
        path: 'anAttribute'
      };

      beforeEach(() => {
        validateContent.mockReturnValue(expectedError);
        truckRepo.findTruckByNumberPlate.mockImplementation(() => undefined);
      });

      it(`should return status ${
        HTTPStatus.BAD_REQUEST
      } with the error information`, async () => {
        await updateTruck(req, res);
        expect(res.status).toHaveBeenCalledWith(HTTPStatus.BAD_REQUEST);
        expect(res.send).toHaveBeenCalledWith({
          errors: expectedError
        });
      });
    });

    describe('when number plate is already in use', () => {
      const expectedRepoResp = {
        id: '1',
        numberPlate: 'XXX-111',
        regoDate: '2001-01-01',
        tonnes: 24,
        truckType: 'TIPPER'
      };

      beforeEach(() => {
        validateContent.mockReturnValue([]);
        truckRepo.findTruckByNumberPlate.mockImplementation(
          () => expectedRepoResp
        );
      });

      it(`should return ${
        HTTPStatus.BAD_REQUEST
      } with numberPlate error`, async () => {
        await updateTruck(req, res);
        expect(res.status).toHaveBeenCalledWith(HTTPStatus.BAD_REQUEST);
        expect(res.send).toHaveBeenCalledWith({
          errors: [
            {
              detail: '"numberPlate" must be unique',
              path: 'numberPlate'
            }
          ]
        });
        expect(truckRepo.update).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('when validations passes but the update operation has a success', () => {
    beforeEach(() => {
      truckRepo.findTruckByNumberPlate.mockImplementation(() => {
        return null;
      });

      truckRepo.update.mockImplementation(() => {
        throw new Error('Something went wrong');
      });
    });

    it('should return an internal server error', async () => {
      await updateTruck(req, res);

      expect(res.sendStatus).toHaveBeenCalledWith(
        HTTPStatus.INTERNAL_SERVER_ERROR
      );
    });
  });

  describe('when validations passes and the update operation is a success', () => {
    const truck = {
      id: 1,
      ...req.body
    };

    beforeEach(() => {
      truckRepo.findTruckByNumberPlate.mockImplementation(() => {
        return null;
      });

      truckRepo.update.mockImplementation(() => truck);
    });

    it('should return the updated truck', async () => {
      await updateTruck(req, res);

      expect(res.status).toHaveBeenCalledWith(HTTPStatus.OK);
      expect(res.send).toHaveBeenCalledWith(truck);
    });
  });
});
