import HTTPStatus from 'http-status';

import truckRepo from '../../db/truckRepo';

const getTruck = async (req, res) => {
  const id = req.params.id;
  req.log.debug(`Received request to fetch truck with ID ${id}`);
  try {
    const truck = await truckRepo.findTruckById(id);

    if (truck) {
      return res.status(HTTPStatus.OK).send(truck);
    }

    return res.sendStatus(HTTPStatus.NOT_FOUND);
  } catch (err) {
    req.log.error(
      `Error while getting truck from the database, ${err.message}`
    );
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default getTruck;
