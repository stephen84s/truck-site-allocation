import HTTPStatus from 'http-status';
import moment from 'moment';

import truckRepo from '../../db/truckRepo';
import siteRepo from '../../db/siteRepo';
import truckSiteRepo from '../../db/truckSiteRepo';
import TruckSite from '../../model/TruckSite';

const validate = req => {
  const allocationDate = req.body.allocationDate;

  const validationErrors = [];
  if (!allocationDate || !moment(allocationDate, 'YYYY-MM-DD').isValid()) {
    validationErrors.push({
      detail: '"allocationDate" is not valid',
      path: 'allocationDate'
    });
  }

  return validationErrors;
};

const generateNotFoundErrors = (site, truck) => {
  const notFoundErrors = [];
  if (!site) {
    notFoundErrors.push({
      detail: '"siteId" is not found',
      path: 'siteId'
    });
  }

  if (!truck) {
    notFoundErrors.push({
      detail: '"truckId" is not found',
      path: 'truckId'
    });
  }

  return notFoundErrors;
};

const createUpdateAllocation = async (req, res) => {
  const { siteId, truckId } = req.params;

  try {
    let validationErrors = validate(req);
    const site = await siteRepo.findSiteById(siteId);
    const truck = await truckRepo.findTruckById(truckId);

    if (!site || !truck) {
      validationErrors = [
        ...validationErrors,
        ...generateNotFoundErrors(site, truck)
      ];
    }

    if (validationErrors.length) {
      return res.status(HTTPStatus.NOT_FOUND).send({
        errors: validationErrors
      });
    }

    const allocationDate = req.body.allocationDate;
    const truckSite = new TruckSite(truck, site, allocationDate);

    const alreadyAllocatedTruckSite = await truckSiteRepo.findTruckSiteByTruckId(
      truckId
    );

    if (
      alreadyAllocatedTruckSite &&
      alreadyAllocatedTruckSite.siteId === site.id
    ) {
      await truckSiteRepo.update(truckSite);
    } else {
      const trucksAllocatedToTheSite = await truckSiteRepo.findTrucksAllocatedToSite(
        siteId
      );
      if (
        trucksAllocatedToTheSite &&
        trucksAllocatedToTheSite.length === site.capacity
      ) {
        return res.status(HTTPStatus.BAD_REQUEST).send({
          errors: [
            {
              detail: 'site is already at capacity'
            }
          ]
        });
      }

      if (alreadyAllocatedTruckSite) {
        await truckSiteRepo.update(truckSite);
      } else {
        await truckSiteRepo.add(truckSite);
      }
    }

    return res.status(HTTPStatus.OK).send({
      truck,
      site,
      allocationDate
    });
  } catch (err) {
    req.log.error(
      `Error while adding/updating TruckSites in database, ${err.message}`
    );
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default createUpdateAllocation;
