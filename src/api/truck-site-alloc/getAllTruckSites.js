import HTTPStatus from 'http-status';

import truckSiteRepo from '../../db/truckSiteRepo';

const getAllTruckSites = async (req, res) => {
  try {
    const truckSites = await truckSiteRepo.findAllTruckSites();

    return res.status(HTTPStatus.OK).send(truckSites);
  } catch (err) {
    req.log.error(`Error while retrieving all truck sites, ${err.message}`);
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default getAllTruckSites;
