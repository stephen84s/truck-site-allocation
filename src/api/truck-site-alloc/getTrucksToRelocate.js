import HTTPStatus from 'http-status';
import moment from 'moment';

import truckSiteRepo from '../../db/truckSiteRepo';

const getTrucksToRelocate = async (req, res) => {
  const date = req.query.date;
  if (!date || !moment(date, 'YYYY-MM-DD', true).isValid()) {
    return res.status(HTTPStatus.BAD_REQUEST).send({
      errors: [
        {
          detail: '"date" is invalid',
          path: 'date'
        }
      ]
    });
  }

  try {
    const trucks = await truckSiteRepo.findTrucksToReallocate(date);

    return res.status(HTTPStatus.OK).send({
      trucks
    });
  } catch (err) {
    req.log.error(`Error while retrieving trucks to relocate, ${err.message}`);
    return res.sendStatus(HTTPStatus.INTERNAL_SERVER_ERROR);
  }
};

export default getTrucksToRelocate;
