class Truck {
  constructor({ id, numberPlate, regoDate, tonnes, truckType }) {
    this.id = id;
    this.numberPlate = numberPlate;
    this.regoDate = regoDate;
    this.tonnes = tonnes;
    this.truckType = truckType;
  }
}

export default Truck;
