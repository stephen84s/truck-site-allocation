class TruckSite {
  constructor(truck, site, allocationDate) {
    this.truck = truck;
    this.site = site;
    this.allocationDate = allocationDate;
  }
}

export default TruckSite;
