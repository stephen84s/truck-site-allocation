class Site {
  constructor({ id, name, location: { latitude, longitude }, capacity }) {
    this.id = id;
    this.name = name;
    this.location = { latitude, longitude };
    this.capacity = capacity;
  }
}

export default Site;
