import express from 'express';

import HTTPStatus from 'http-status';

import getTruck from './api/trucks/getTruck';
import getTrucks from './api/trucks/getTrucks';
import createTruck from './api/trucks/createTruck';
import updateTruck from './api/trucks/updateTruck';
import deleteTruck from './api/trucks/deleteTruck';
import createSite from './api/sites/createSite';
import getSites from './api/sites/getSites';
import getSite from './api/sites/getSite';
import updateSite from './api/sites/updateSite';
import deleteSite from './api/sites/deleteSite';
import modifyTruckSite from './api/truck-site-alloc/createUpdateAllocation';
import getTrucksToRelocate from './api/truck-site-alloc/getTrucksToRelocate';
import getAllTruckSites from './api/truck-site-alloc/getAllTruckSites';

const route = express.Router();

route.get('/health-check', (_req, res) => {
  return res.status(HTTPStatus.OK).send('OK');
});

route.get('/trucks/:id', getTruck);
route.get('/trucks', getTrucks);
route.post('/trucks', createTruck);
route.put('/trucks/:id', updateTruck);
route.delete('/trucks/:id', deleteTruck);

route.get('/sites/:id', getSite);
route.get('/sites', getSites);
route.post('/sites', createSite);
route.put('/sites/:id', updateSite);
route.delete('/sites/:id', deleteSite);

route.post('/trucks/:truckId/sites/:siteId', modifyTruckSite);
route.get('/trucksToRelocate', getTrucksToRelocate);
route.get('/truckSites', getAllTruckSites);

export default route;
