import createServer from './server';
import logger from './util/logger';
import { DEFAULT_PORT } from './constants';
import daoLayer from './db/daoLayer';

const server = createServer(logger);
const port = process.env.PORT || DEFAULT_PORT;

const init = async () => {
  try {
    await daoLayer.init();
    logger.info('Database successfully initialised');

    server.on('error', e => {
      logger.error(
        `Encountered error while starting server, cause: ${JSON.stringify(e)}`
      );
    });

    server.listen(port, () => {
      logger.info(`Started server on port ${port}`);
    });
  } catch (err) {
    logger.error('Could not start application, error: ', err);
  }
};

init();

export default server;
