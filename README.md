# Truck Parking

## The Problem

### Actors

- Trucks
  - Attributes
    - NumberPlate (XXX-111)
    - Type (Tipper, Tanker, Agitator)
    - Tonnes
    - RegoDate
- Sites
  - Attributes
    - ID
    - Name
    - Location (Latititude, Logitude)
    - NoOfParkingSpaces

## Rules

- Trucks can't stay on a site longer than 2 days
- Trucks can't be allocated to a site with no parking space

### Assumptions

- A truck can't be reallocated to the same site on the same date
- We only take dates, no time is considered

## Endpoints Specifications

### Create Truck

   1. POST
   1. `/trucks`
   1. Request Body

      ```JSON
      {
          "numberPlate": "XXX-118",
          "regoDate": "2018-01-01",
          "tonnes": 499,
          "truckType": "TIPPER"
      }
      ```

   1. Responses
      1. Sucess
         1. Status: 201
         1. Response Body: The created truck with ID

            ```JSON
            {
              "id": 1,
              "numberPlate": "XXX-118",
              "regoDate": "2018-01-01",
              "tonnes": 499,
              "truckType": "TIPPER"
            }
            ```

      1. Failure:
         1. Status: 400
         1. Response Body: The errors in the given request body

            Sample Body:-

            ```JSON
            {
              "errors": [
                {
                  "detail": "\"truckType\" must be one of [AGITATOR, TANKER, TIPPER]",
                  "path": "truckType"
                },
                {
                  "detail": "\"tonnes\" must be larger than or equal to 5",
                  "path": "tonnes"
                }
              ]
            }
            ```

            Other possible errors: Number plate already in use, regoDate is invalid.

         1. Status: 500
            Response Body: Blank
            Reason: Given back when there is an unhandled error on server side.

### Get All Trucks

1. GET
1. `/trucks`
1. Success Response:-
   1. Status: 200
   1. Body

      ```JSON
      [
        {
          "id": 1,
          "numberPlate": "XXX-118",
          "regoDate": "2018-01-01",
          "tonnes": 499,
          "truckType": "TIPPER"
        }, {
          "id": 2,
          "numberPlate": "XXX-119",
          "regoDate": "2018-01-02",
          "tonnes": 499,
          "truckType": "AGITATOR"
        }, {
          "id": 3,
          "numberPlate": "XXX-120",
          "regoDate": "2018-01-02",
          "tonnes": 499,
          "truckType": "TANKER"
        }
      ]
      ```

### Get truck

1. GET
1. `/trucks/<TRUCK_ID>`
1. Success Response:-
   1. Status: 200
   1. Body

      ```JSON
      {
          "id": 1,
          "numberPlate": "XXX-118",
          "regoDate": "2018-01-01",
          "tonnes": 499,
          "truckType": "TIPPER"
      }
      ```

1. Failure Responses:-
   1. Status: 404
      1. Body: empty
      1. Reason: Truck not found
   1. Status: 500
      1. Body: Empty
      1. Reason: Server error, probably fetching from db threw an error we could not recover from.

### Update Truck

1. PUT
1. `/trucks/<TRUCK_ID>`
1. Request Body: (All Truck Data)

  ```JSON
  {
    "id": 1,
    "numberPlate": "XXX-115",
    "regoDate": "2018-01-01",
    "tonnes": 499,
    "truckType": "TIPPER"
  }
  ```

1. Success Response
   1. Status: 200
   1. Response Body: Same as the request
1. Errors: Same errors as for Create.

### Delete Truck

1. DELETE
1. `/trucks/<TRUCK_ID>`
1. Responses:
   1. 204: Successfully Deleted
   1. 404: Truck not found

### Get All Sites

1. GET
1. `/sites`
1. Status: 200
1. Sample Response Body:

  ```JSON
  [
    {
        "id": 1,
        "name": "Site 1",
        "location": {
            "latitude": 10,
            "longitude": 20
        },
        "capacity": 2
    },
    {
        "id": 2,
        "name": "Site 2",
        "location": {
            "latitude": 30,
            "longitude": 40
        },
        "capacity": 1
    }
  ]
  ```

### Create Site

1. POST
1. `/site`
1. Request Body:

  ```JSON
  {
    "name": "Alpha",
    "capacity": 499,
    "location": {
      "latitude": 1.1,
      "longitude": -1.1
    }
  }
  ```

  Note: There are no validations on name to be unique

1. Responses
   1. Success
      1. Status: 200
      1. Sample body:

        ```JSON
        {
          "id": 1,
          "name": "Alpha",
          "location": {
              "latitude": 1.1,
              "longitude": -1.1
          },
          "capacity": 499
        }
        ```

   1. Failure
      1. Status: 400
      1. Reasons:
         1. name is empty
         1. capacity is invalid
         1. latitude / longitude are invalid
      1. Sample error body:

        ```JSON
        {
          "errors": [
            {
              "detail": "\"capacity\" must be a number",
              "path": "capacity"
            },
            {
              "detail": "\"latitude\" must be a number",
              "path": "location.latitude"
            }
          ]
        }
        ```

### Update Site

1. PUT
1. `/sites/<SITE_ID>`
1. Request Body: (All Site Data)

  ```JSON
  {
    "id": 1,
    "name": "Alpha",
    "location": {
        "latitude": 1.1,
        "longitude": -1.1
    },
    "capacity": 499
  }
  ```

1. Success Response
   1. Status: 200
   1. Response Body: Same as the request
1. Errors: Same errors as for Create.

### Delete Site

1. DELETE
1. `/sites/<SITE_ID>`
1. Responses:
   1. 204: Successfully Deleted
   1. 404: Site not found

1. Allocate Truck to a Site on a given date
1. POST
1. `/trucks/<TRUCK_ID>/sites/<SITE_ID>`
1. Request Body

  ```JSON
  {
    "allocationDate": "YYYY-MM-DD"
  }
  ```

1. Success Response
   1. Status: 200
   1. Body: (Includes the Truck and Site it was allocated to)

      ```JSON
      {
        "truck": {
          "id": 1,
          "numberPlate": "XXX-118",
          "regoDate": "2018-01-01",
          "tonnes": 499,
          "truckType": "TIPPER"
        },
        "site": {
          "id": 1,
          "name": "x",
          "location": {
            "latitude": 1.1,
            "longitude": 1.1
          },
          "capacity": 100
        },
        "allocationDate": "2018-01-10"
      }
      ```

   1. Errors
      1. Status: 404
         1. Reason: Either TruckId or SiteId not found.
         1. Sample response:

            ```JSON
            {
              "errors": [
                {
                  "detail": "\"siteId\" is not found",
                  "path": "siteId"
                },
                {
                  "detail": "\"truckId\" is not found",
                  "path": "truckId"
                }
              ]
            }
            ```

### Get All Truck and Site Allocations

1. GET
1. `/truckSites`
1. Success Response
   1. Status: 200
   1. Sample Response Body:

      ```JSON
      [
        {
          "truck": {
            "numberPlate": "XXX-118",
            "regoDate": "2018-01-01",
            "tonnes": 499,
            "truckType": "TIPPER"
          },
          "site": {
            "id": 1,
            "name": "Alpha",
            "location": {
              "latitude": 1.1,
              "longitude": -1.1
            },
            "capacity": 499
          },
          "allocationDate": "2018-01-13"
        },
        {
          "truck": {
            "numberPlate": "XXX-119",
            "regoDate": "2018-01-01",
            "tonnes": 499,
            "truckType": "TIPPER"
          },
          "site": {
            "id": 3,
            "name": "Alpha",
            "location": {
              "latitude": 1.1,
              "longitude": -1.1
            },
            "capacity": 499
          },
          "allocationDate": "2018-01-13"
        },
        {
          "truck": {
            "numberPlate": "XXX-120",
            "regoDate": "2018-01-01",
            "tonnes": 499,
            "truckType": "TIPPER"
          },
          "site": {
            "id": 2,
            "name": "Alpha",
            "location": {
              "latitude": 1.1,
              "longitude": -1.1
            },
            "capacity": 499
          },
          "allocationDate": "2018-01-13"
        }
      ]
      ```

### Given a date which trucks need to be allocated to new site

1. GET
1. `/trucksToRelocate?date=<DATE_IN_YYYY-MM-DD_FORMAT>` ex: `/trucksToRelocate?date=2019-10-01`
1. Success Response
   1. Status: 200
   1. Sample Response Body

    ```JSON
    {
      "trucks": [
        {
          "id": 1,
          "numberPlate": "XXX-118",
          "regoDate": "2018-01-01",
          "tonnes": 499,
          "truckType": "TIPPER"
        }
      ]
    }
    ```

## Configuration

The following environment variables can be used to configure the application:-

| *Environment Variable* | *Configures* | *Default Value* |
| ---------------------- | ------------ | --------------- |
| PORT | Port on which the API listens | 3000 |
| DB_FILE | File where the database is stored | trucks.sqlite |

## Running the Code

### Prerequisites

- [node 11.10](https://nodejs.org/en/)
- [yarn](https://yarnpkg.com/en/)
- [docker](https://www.docker.com/products/docker-desktop) (To run via docker)

### Getting working with node

If you already have [nvm](https://github.com/creationix/nvm) (or [nvm-windows](https://github.com/coreybutler/nvm-windows) for windows) installed, you can run the following commands to download and install the correct version of node:-

```bash
nvm install
nvm use
```

If on windows and using nvm-windows, install and run with the following commands:-

```bash
nvm install 11.10.0
nvm use 11.10.0
```

#### Yarn

Following command installs `yarn`:-

```bash
npm install -g yarn
```

### Install Dependencies

To install dependencies just run:-

```bash
yarn
```

### Running Tests

To run the tests run:-

```bash
yarn test
```

### Postman Collections

[Postman](https://www.getpostman.com/) can be used to test the API.
The collections to test the API are included in the [postman](/postman) folder and can be directly imported into the Postman App.

### Starting Application

#### Docker

To run the application without compiling any sources run:-

```bash
docker container run --name=truck-site-parking -d -p 3000:3000 -e PORT=3000 -e LOG_LEVEL=debug stephen84s/truck-site-parking:0.0.1
```

To run the application via docker:-

```bash
docker-compose up
```

To start the application in daemon mode:-

```bash
docker-compose up -d
```

Once started the API should be available on [http://localhost:3000](http://localhost:3000).

To stop the application :-

```bash
docker-compose stop
```

To stop and clear the images

```bash
docker-compose down
```

To rebuild the docker images:-

```bash
docker-compose build
```

#### Natively

The application has been tested with node 11.10.0 and yarn 1.10.1.
To start the application in dev mode :-

```bash
yarn start.dev
```

To run the application in production mode, you need to first build the application and then run it:-

```bash
yarn build
yarn start
```

Once started the application should be available at [http://localhost:3000](http://localhost:3000).
